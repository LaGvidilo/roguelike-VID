#coding: utf-8
import key
clavier = key.key()

import platform
import os

class bcolors:
	HEADER = '\033[95m'
	OKBLUE = '\033[94m'
	OKGREEN = '\033[92m'
	WARNING = '\033[93m'
	FAIL = '\033[91m'
	ENDC = '\033[0m'
	BOLD = '\033[1m'
	UNDERLINE = '\033[4m'

def Clean():
    if platform.system() == "Windows":
        os.system("cls")
    else:
        os.system("clear")

K = 0
acts = ["OPTION 1","OPTION 2","ACTION","ANNULER"]
while True:
	Clean()
	for i in acts:
		if acts.index(i) == K:
			print bcolors.HEADER+i+bcolors.ENDC
		else:
			print i
	TOUCHE = clavier.getkey()
	if TOUCHE == "down": K+=1
	if TOUCHE == "up": K-=1

	if K < 0: K = 0
	if K > len(acts)-1: K = len(acts)-1 

	if (TOUCHE == "start") or (TOUCHE == "select"): 
		if K == 3: return (K,acts[K])