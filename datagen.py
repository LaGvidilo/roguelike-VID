#coding: utf-8

import random
import datetime
from string import ascii_lowercase



class Generator(object):
	class random_lang(object):
		def __init__(self,seed=7):
			self.VOWELS = ['a', 'e', 'i', 'o', 'u']
			self.CONSONANTS = [ c for c in ascii_lowercase if c not in self.VOWELS] + ['']*2
			self.seed = seed
			self.init_seed()
			f=open("GENRESS/wordfr.txt",'r+b')
			self.wordfr = str(f.read()).split("\n")
			f.close()

		def init_seed(self):
			random.seed(self.seed)

		def random_word(self):
			""" Returns a random word composed of consonants and vowels """
			word = ''
			random_range = random.randint(3,4)  
			for i in range(random_range):
				word += random.choice(self.VOWELS) + random.choice(self.CONSONANTS)
			return word

		def unrealist_langage(self,seed=7,wordsnum=200):
			self.seed = seed
			self.init_seed()
			self.words = []
			for i in range(0,wordsnum):
				self.words.append(self.random_word())

		def unrealist_speak(self,id_perso,len_dial,max_phrase=9):
			speak = {}
			id_=0
			for i in range(0,len_dial):

				phrase = []
				for j in range(0,max_phrase):
					phrase.append(random.choice(self.words))


				
				rep = {}
				for j in range(0,4):
					p = []
					for jp in range(0,max_phrase-2):
						p.append(random.choice(self.words))
					rep[" ".join(p)] = "bad"

				rep[random.choice(self.words)] = "good"


				bad = []
				for j in range(0,max_phrase-1):
					bad.append(random.choice(self.words))

				speak[id_] = {}
				speak[id_]['text'] = " ".join(phrase)
				speak[id_]['reponses'] = rep

				if i-1 >= len_dial-1:
					speak[id_]['reponses'][max(speak[id_]['reponses'].iteritems())[0]] = "kill"

				speak[id_]['bad'] = " ".join(bad)
				id_+=1

			final = {}
			final[id_perso] = {
				'current':0,
				'dials': speak
			}

			
			return final

		def generator_unreal(self,id_,_mindial_=3,_maxdial_=10,seed=7,numwords=200):
			self.unrealist_langage(seed,numwords)
			return self.unrealist_speak(id_,len_dial=random.randint(_mindial_,_maxdial_),max_phrase=9)


	class PNJ(object):
		def __init__(self,id_,lendial=8):
			self.id,self.lendial=id_,lendial

