def cheat_pfc(choix):
	if choix == 0:
		return 1
	if choix == 1:
		return 2
	if choix == 2:
		return 0

def sample(p):
	if random.random() < p:
		return 1
	else:
		return 0

import ast
def jsonKeys2str(x):
	if isinstance(x, dict):
		return {int(k):v for k,v in x.items()}
	return x
def loader(filename):
	try:
		f = open("gamedata/"+filename,'r+b')
		DATA = f.read()
		f.close()
		DATA = DATA.replace("null","None").replace("true","True").replace("false","False").decode('ISO-8859-2')
		DATA = ast.literal_eval(DATA)
	except:
		f = open("gamedata/"+filename,'r')
		DATA = f.read()
		f.close()
		DATA = DATA.replace("null","None").replace("true","True").replace("false","False")
		DATA = ast.literal_eval(DATA)

	try:
		DATA = jsonKeys2str(DATA)
	except:
		pass

	DICTIONNAIRE = dict(DATA)
	for i in DICTIONNAIRE.keys():
		try:
			if "inventaire" in DICTIONNAIRE[i].keys():
				print( DICTIONNAIRE[i].keys())
				print( DICTIONNAIRE[i]['inventaire'])
				DICTIONNAIRE[i]['inventaire'] = jsonKeys2str(DICTIONNAIRE[i]['inventaire'])
		except:
			pass

		try:
			if "dials" in DICTIONNAIRE[i].keys():
				print( DICTIONNAIRE[i].keys())
				print( DICTIONNAIRE[i]['dials'])
				DICTIONNAIRE[i]['dials'] = jsonKeys2str(DICTIONNAIRE[i]['dials'])
		except:
			pass


	return DICTIONNAIRE