"""
This is a setup.py script generated by py2applet

Usage:
    python setup.py py2app
"""

from setuptools import setup
import FluoSQL as fluo

loader = fluo.autoload()
a1=loader.load_names_dict('gamedata')
a2=loader.load_names_dict('gameressource')

APP = ['main.py']
DATA_FILES = ["asciinator.py","datagen.py","FluoSQL.py","key.py","main.py"]
for i in a1:
	DATA_FILES.append(i)
for i in a2:
	DATA_FILES.append(i)

includes = ['py2app','sys','terminaltables','os','platform','pygame','ast','random','datetime','math']
OPTIONS = {"includes" : includes,
            'plist': {'LSUIElement': True},
            'argv_emulation': True}
setup(
    app=APP,
    data_files=DATA_FILES,
    options={'py2app': OPTIONS},
    setup_requires=['py2app']
)