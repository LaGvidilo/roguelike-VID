# adapted from https://github.com/recantha/EduKit3-RC-Keyboard/blob/master/rc_keyboard.py
 


import sys, termios, tty, os, time
class key(object):
	def getch(self):
		fd = sys.stdin.fileno()
		old_settings = termios.tcgetattr(fd)
		try:
			tty.setraw(sys.stdin.fileno())
			ch = sys.stdin.read(1)
			if ch == "\x1b": ch = ch + sys.stdin.read(2)

		finally:
			termios.tcsetattr(fd, termios.TCSADRAIN, old_settings)
		return ch


	def __init__(self):
		self.key = ""
		self.knames = {'\x1b[A':"up",'\x1b[B':"down",'\x1b[D':"left",'\x1b[C':"right",'\r':"start",' ':"select"}

	def getkey(self):
		self.key = self.getch()
		if self.key in self.knames.keys():
			self.key = self.knames[self.key]

		return self.key 