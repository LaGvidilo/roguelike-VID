# coding: utf-8

import unicodedata
import ast
def jsonKeys2str(x):
	if isinstance(x, dict):
		return {int(k):v for k,v in x.items()}
	return x
def loader(filename):
	try:
		f = open("gamedata/"+filename,'r+b')
		DATA = f.read()
		f.close()
		DATA = DATA.replace("null","None").replace("true","True").replace("false","False").decode('ISO-8859-2')
		DATA = ast.literal_eval(DATA)
	except:
		f = open(filename,'r+b')
		DATA = f.read()
		f.close()
		DATA = DATA.replace("null","None").replace("true","True").replace("false","False").decode('ISO-8859-2')
		DATA = ast.literal_eval(DATA)

	try:
		DATA = jsonKeys2str(DATA)
	except:
		pass

	DICTIONNAIRE = dict(DATA)
	for i in DICTIONNAIRE.keys():
		try:
			if "inventaire" in DICTIONNAIRE[i].keys():
				#print DICTIONNAIRE[i].keys()
				#print DICTIONNAIRE[i]['inventaire']
				DICTIONNAIRE[i]['inventaire'] = jsonKeys2str(DICTIONNAIRE[i]['inventaire'])
		except:
			pass

		try:
			if "dials" in DICTIONNAIRE[i].keys():
				#print DICTIONNAIRE[i].keys()
				#print DICTIONNAIRE[i]['dials']
				DICTIONNAIRE[i]['dials'] = jsonKeys2str(DICTIONNAIRE[i]['dials'])
		except:
			pass


	return DICTIONNAIRE


dialogues = loader("dialogues.dict")
lieux = loader("lieux.dict")
personnages = loader("personnages.dict")
rick = loader("rick.dict")
generic = loader("generic.dict")

def string_to_float(x):
    z = ""
    for i in x:
        if i in "0123456789.":
            if i == ".":
                if z.count(i)==0:
                    z=z+i
            else:
                z=z+i
    if z=="": z="0.00"
    return float(z)

def string_to_int(x):
    z = ""
    for i in x:
        if i in "0123456789":
            z=z+i
    if z=="": z="0"
    return int(z)


def new_item():
	objets = loader("items.dict")
	R = {}
	print "New item.:"
	R['description'] = raw_input("Name: ")
	rep = raw_input("Is Consommable?(Yes/No): ")
	if "y" in rep.lower():
		R['consommable'] = True
	else:
		R['consommable'] = False

	rep = raw_input("Is Usable?(Yes/No): ")
	if "y" in rep.lower():
		R['use'] = True
	else:
		R['use'] = False

	rep = raw_input("Is A Weapon?(Yes/No): ")
	if "y" in rep.lower():
		R['weapon'] = True
	else:
		R['weapon'] = False

	R['price'] = string_to_int(raw_input("Price: "))

	next_id = max(objets.keys())+1
	objets[next_id] = R
	write_file_dict("gamedata/items.dict",objets)

def define_craft():
	items = loader("items.dict")
	craft = loader("craft.dict")
	R = {}
	for i in craft.keys():
		del items[i]
	for i in items.keys():
		print items[i]['description']
		rep = raw_input("Craftable?(Yes/No):")
		if "n" in rep.lower():
			del items[i]
		if "y" in rep.lower():
			A = raw_input("Objet 1(id):")
			B = raw_input("Objet 2(id):")
			C = raw_input("Objet 3(id):")
			if C != "":
				F = [int(A),int(B),int(C)]
			else:
				F = [int(A),int(B)]
			craft[i] = F
			write_file_dict("gamedata/craft.dict",craft)
	

import itertools
def get_all_possible(liste,n=3):
	return list(itertools.combinations(liste,n))

import json
def write_file_dict(fichier,data):
	buff = json.dumps(data, indent=4, separators=(',', ': '))
	f = open(fichier,'w+b')
	f.write(buff)
	f.close()

import random
import datetime
from faker import Faker
class generator(object):
	def name(self):
		random.seed(datetime.datetime.now())
		lang=['de_DE','en_AU','en_CA','en_GB','en_US','fr_FR','it_IT']
		LNG = random.choice(lang)
		fake = Faker(LNG)
		return fake.name()

	def race(self):
		RACE = ['xinomorphien','humain','plopigrien','frlopigrien']
		return random.choice(RACE)

	def inventaire(self):
		R = {}
		objets_ = [3,2,1,22,20,19,18,17,15,14,13,4]
		LEN = random.randint(0,4)
		for i in range(0,LEN):
			obj = random.choice(objets_)
			if obj in R.keys():
				R[obj] = R[obj] + 1
			else:
				R[obj] = 1
		return R

	def pnj(self):
		"""
		'nom': 'Xiniz',
		'PV': 50,
		'schmeckles': 120,
		'inventaire': { 3:1, 4:1, 9:1 },
		'race': 'plopigrien',
		'atk': 7,
		'def': 12,
		'quete': None
		"""
		pnj = {}
		pnj['nom'] = unicodedata.normalize('NFKD', self.name()).encode('ascii', 'ignore')
		pnj['PV'] = random.randint(10,200)
		pnj['schmeckles'] = random.randint(0,50+pnj['PV']*random.randint(0,12))
		pnj['inventaire'] = self.inventaire()
		pnj['race'] = self.race()
		pnj['atk'] = random.randint(1,12)+random.randint(0,3)*random.randint(0,30)
		pnj['def'] = random.randint(1,12)+random.randint(0,3)*random.randint(0,30)
		pnj['quete'] = None
		return pnj

	def dim_name(self):
		car = "azertyuiopmlkjhgfdsqnbvcxw".upper()
		name = ""
		for i in range(0,random.randint(1,3)):
			name=name+random.choice(car)
		name=name+"-"+str(random.randint(1,999))
		return name

	def generate_new_dim(self):
		"""
		"XY-422":{
			'bar':{
				'generic':0,
				'pnj': []
				},
			'preteur sur gage':{
				'generic':5,
				'pnj': []
				},
			'forêt':{
				'generic':6,
				'pnj': []
				}
		}
		"""
		end_names = ['bar','television','a table','dans un coin','distributeur recreatif','preteur sur gage']
		end_codes = [0,1,2,-1,4,5]
		name = self.dim_name()
		lieux[name] = {}
		for i in range(0,random.randint(1,3)):	
			choix = random.choice(end_names)
			pos = end_names.index(choix)
			lieux[name][choix] = {"generic":end_codes[pos],"pnj":[]}
			for j in range(0,random.randint(1,4)):
				lieux[name][choix]['pnj'].append(self.generate_new_pnj())

	def generate_new_pnj(self):
		id_ = max(personnages.keys())+1
		personnages[id_] = self.pnj()
		dialogues[id_] = {}
		dialogues[id_]['current'] = 0
		dialogues[id_]['dials'] = {}
		for j in range(0,random.randint(1,8)):
			dialogues[id_]['dials'][j] = {"text":"","reponses":{"text1":"good","text2":"bad","text3":"bad"},"bad":"..."}
		return id_

	def save(self):
		write_file_dict("gamedata/personnages.dict",personnages)
		write_file_dict("gamedata/dialogues.dict",dialogues)
		write_file_dict("gamedata/lieux.dict",lieux)

	def generate_world(self,n=15):
		for i in range(0,n):
			self.generate_new_dim()
		self.save()









