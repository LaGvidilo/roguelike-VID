import datetime
import random
#EXPERIMANTAL: NE MARCHE PAS
def func(a,b,c,d):
	#random.seed(str(a)+str(b)+str(c)+str(d))
	TABL = []
	for i in range(0,4):
		TABL.append(i*((a*b+c)-d))
	return TABL

class var:
	RESULT = [8,4,7,9]

from faker import Faker
import unicodedata
from copy import copy
from statistics import mean
class individu(object):
	def name(self):
		lang=['de_DE','en_AU','en_CA','en_GB','en_US','fr_FR','it_IT']
		LNG = random.choice(lang)
		fake = Faker(LNG)
		return fake.name()

	def __init__(self,nbgenome=4):
		self.nbgenome = nbgenome
		self.init_ADN()
		self.name = unicodedata.normalize('NFKD', self.name()).encode('ascii', 'ignore')

	def init_ADN(self):
		j = []
		self.ADN = []
		for i in range(0,self.nbgenome):
			j=j+[random.randint(0,10)]
		self.ADN = j

	def set_ADN(self,ADN):
		self.ADN = ADN

	def evaluate(self,result):
		fnc = "r = func("
		for i in self.ADN:
			fnc = fnc + str(i) + ","
		fnc=fnc[:-1]
		fnc=fnc+")"
		exec(fnc)

		SCORE = 0
		lenr = len(result)
		j=0
		for i in result:
			if i == r[j]:
				SCORE += 1
			j+=1

		SCORE = SCORE / (lenr*1.00)
		print "LE SCORE DE ",self.name,", EST DE:",SCORE
		return SCORE

	def sample(self,p):
		if random.random() < p:
			return 1
		else:
			return 0

	def mutate(self,chance=.1):
		j=0
		for i in self.ADN:
			if self.sample(chance):
				print "ADN DE ",self.name,":",self.ADN
				self.ADN[j] = self.ADN[j] + random.randint(-1,1)
				print "(MUTATION)ADN DE ",self.name,":",self.ADN,"\n"
			j+=1

	def reproduction(self,indivB):
		project = copy(self.ADN)
		ADN = indivB.ADN
		NB = random.randint(1,len(ADN))
		for i in ADN:
			l = ADN.index(i)
			if self.sample(.1):
				project[l] += (-(random.randint(-1,1))) * i
		return project


class population(object):
	def __init__(self,n=100,nbgenome=4):
		self.nbgenome = nbgenome
		self.populas = []
		for i in range(0,n):
			self.born()

	def born(self):
		self.populas.append(individu(self.nbgenome))

	def death(self,seuil=0.00):
		SCRS= []
		for i in self.populas:
			SCORE = i.evaluate(var.RESULT)
			SCRS.append(SCORE)

		min_,moy_,max_ = min(SCRS),mean(SCRS),max(SCRS)
		seuil = moy_

		for i in self.populas:
			if i.evaluate(var.RESULT) <= seuil:
				print self.populas[self.populas.index(i)].name, "EST MORT."
				del self.populas[self.populas.index(i)]
		print "POPULATION: ",len(self.populas)

	def mutations(self,chance=.01):
		for i in self.populas:
			i.mutate(chance)

	def evaluate(self):
		SCRS= []
		for i in self.populas:
			SCORE = i.evaluate(var.RESULT)
			SCRS.append(SCORE)
		print "POPULATION: ",len(self.populas)
		return max(SCRS)

	def get_ADN(self):
		ADN= []
		for i in self.populas:
			SCORE = i.evaluate(var.RESULT)
			if SCORE >= self.nbgenome:
				ADN = i.ADN
		print "POPULATION: ",len(self.populas)
		return ADN

	def repro(self):
		j=0
		POP = []
		for i in self.populas:
			print "REPRODUCTION(",i.name,")","STEP:",str(j)+"/"+str(len(self.populas))
			ADN = i.reproduction(random.choice(self.populas))
			IND =individu(self.nbgenome)
			IND.set_ADN(ADN)
			POP.append(IND)
			j+=1

		for i in POP:
			self.populas.append(i)

		print self.evaluate()

class farm(object):
	def __init__(self,pop=100,muta=.05,nbgen=4):
		self.popu = population(pop,nbgen)
		self.nbgen,self.muta=nbgen,muta


	def loop(self):
		Z = False
		while not Z:
			Z = (self.popu.evaluate()>=self.nbgen)
			self.popu.death()
			self.popu.mutations()
			self.popu.repro()

		ADN = self.popu.get_ADN()
		print "ADN SOLUTION:", ADN






