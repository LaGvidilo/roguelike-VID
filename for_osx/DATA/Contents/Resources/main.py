#coding: utf-8
"""
python -m pip install terminaltables

"""
import sys
reload(sys)
sys.setdefaultencoding("utf-8")

#pour afficher des tableaux(ex: l'inventaire)
from terminaltables import AsciiTable
import os
import platform
def Clean():
    if platform.system() == "Windows":
        os.system("cls")
    else:
        os.system("clear")

import pygame
pygame.mixer.init()
Clean()
class AudioMotor(object):
	class Music(object):
		def LoadPlay(self,filepath):
			pygame.mixer.music.load(filepath)
			pygame.mixer.music.play()
		def Stop(self):
			pygame.mixer.music.stop()

AUDIO = AudioMotor()

import ast
def loader(filename):
	path="gamedata/"
	try:
		f = open(path+filename,'r+b')
		DATA = f.read()
		f.close()
		return ast.literal_eval(DATA)
	except:
		f = open(filename,'r+b')
		DATA = f.read()
		f.close()
		return ast.literal_eval(DATA)

dialogues = loader("dialogues.dict")
objets = loader("items.dict")
lieux = loader("lieux.dict")
personnages = loader("personnages.dict")
rick = loader("rick.dict")
generic = loader("generic.dict")

BLOCKED = {'XZ-321':'dans un coin'}

INTRO="""
	Vous venez d'arriver dans la dimension XZ-321 en passant par un trou de vers a bord de votre vaisseau.\n
	Votre mission et de retrouver votre pisto-portail, ramener Jerry sur la terre de D-634.\n
	Votre vaisseaux rentrera sur D-634 seul grace au pilotage automatique.
	"""

class bcolors:
	HEADER = '\033[95m'
	OKBLUE = '\033[94m'
	OKGREEN = '\033[92m'
	WARNING = '\033[93m'
	FAIL = '\033[91m'
	ENDC = '\033[0m'
	BOLD = '\033[1m'
	UNDERLINE = '\033[4m'

import random
from datetime import datetime
import math
#import os
#os.system('cls||clear')
class Game(object):
	def __init__(self):
		self.position = None

	def action(self,choix,rnd=0):
		if rnd:
			random.shuffle(choix)
		while True:
			for i in choix:
				print choix.index(i),"-",i

			c = raw_input("Votre choix: ")
			try:
				c=int(c)
			except:
				c=-3
			if  -1 < c <= len(choix):
				return (c,choix[c])

	def social(self,choix):
		while True:
			for i in choix:
				print choix.index(i),"-",personnages[i]['nom']

			c = raw_input("Parler à: ")
			try:
				c=int(c)
			except:
				c=-3
			if  -1 < c <= len(choix):
				return (choix[c]-1,personnages[choix[c]]['nom'])

	def get_local_move(self):
		try:
			BLCKD = BLOCKED[rick['in']]
		except:
			BLCKD = None 

		ACT = []
		for i in lieux[rick['in']].keys():
			if i != BLCKD:
				ACT.append(i)

		return self.action(ACT)

	def get_local_social(self):
		if self.position!=None: return self.social(lieux[rick['in']][self.position]['pnj'])

	def menu_root(self):
		gnc = None
		if rick['PV']<=0: self.death()
		print "VOUS ETES DANS LA DIMENSION: ", bcolors.BOLD+str(rick['in'])+bcolors.ENDC
		if self.position!=None: print "VOUS ETES A LA POSITION:",bcolors.BOLD+str(self.position)+bcolors.ENDC
		print "VOUS POUVEZ VOUS DEPLACER."
		print "VOUS POUVEZ UTILISER VOTRE INVENTAIRE."
		if rick['PV']['current']<rick['PV']['max']/2.0:
			print "VOUS AVEZ",bcolors.WARNING+str(rick['PV']['current'])+bcolors.ENDC,"/",rick['PV']['max'],"PV"#'PV': {'current':19,'max':25}
		else:
			print "VOUS AVEZ",rick['PV']['current'],"/",rick['PV']['max'],"PV"#'PV': {'current':19,'max':25}
	
		print "VOUS ETES NIVEAU:",bcolors.HEADER+str(rick['level'])+bcolors.ENDC
		print "VOUS AVEZ:",rick['schmeckles'], "SCHMECKLES."

		if self.position!=None: 
			if len(lieux[rick['in']][self.position]['pnj'])>0: 
				print "VOUS POUVEZ DISCUTER."
				actions_list = ["Se deplacer...","Voir l'inventaire...","Discuter..."]
			else:
				actions_list = ["Se deplacer...","Voir l'inventaire..."]
			if lieux[rick['in']][self.position]['generic']!= -1:
				gnc = generic[lieux[rick['in']][self.position]['generic']]
				for i in gnc.keys():
					actions_list.append(i)

		else:
			actions_list = ["Se deplacer...","Voir l'inventaire..."]


		chx_id,chx_name = self.action(actions_list)

		if gnc != None:
			if chx_id > 2:
				#print "<<INFO>>:",chx_id,chx_name
				lgen = lieux[rick['in']][self.position]['generic']
				#print "generic=",generic
				generic_actarg = 'self.'+generic[lgen][chx_name]['func']+'()'
				price = generic[lgen][chx_name]['price']
				money = generic[lgen][chx_name]['money']
				if money == "PV":
					if rick[money]['current']>price:
						print "VOUS PAYEZ: ",price,money
						rick[money]['current']-=price
						exec(generic_actarg)
					else:
						print "VOUS N'AVEZ PAS ASSEZ DE: ",money
				else:
					if rick[money]>=price:
						print "VOUS PAYEZ: ",price,money
						rick[money]-=price
						exec(generic_actarg)
					else:
						print "VOUS N'AVEZ PAS ASSEZ DE: ",money



		if chx_id == 0:
			print "Voici les lieux possibles:"
			mve_id,mve_name = self.get_local_move()
			print "Vous êtes maintenant à:",bcolors.BOLD+str(mve_name)+bcolors.ENDC
			self.position = mve_name

		if chx_id == 1:
			if len(rick['inventaire'].keys())>0:
				print "Voici votre inventaire:"
				RINV = {}
				table_inv = [["CONSOMMABLE","UTILISABLE"]]
				usable_inv = []
				consommable_inv = []
				for inv in rick['inventaire'].keys():
					if objets[inv]['consommable']:
						consommable_inv.append("(x"+str(rick['inventaire'][inv])+")"+str(objets[inv]['description']))
						RINV[objets[inv]['description']] = inv
					if objets[inv]['use']:
						usable_inv.append("(x"+str(rick['inventaire'][inv])+")"+str(objets[inv]['description']))
				table_inv.append(consommable_inv)
				table_inv.append(usable_inv)
				table_=AsciiTable(table_inv)
				print table_.table
						#RINV[objets[inv]['description']] = inv

				print "Voulez vous utiliser quelque chose de votre inventaire?"
				qinv_id,qinv_name = self.action(["Oui","Non"])
				if qinv_id == 0:
					print "Quel objet?"
					inv_id,inv_name = self.action(RINV.keys())
					rick['inventaire'][RINV[inv_name]]-=1
					if rick['inventaire'][RINV[inv_name]]<=0:
						del rick['inventaire'][RINV[inv_name]]
					self.effect_object(RINV[inv_name])
			else:
				print "Votre inventaire est vide!"


		if chx_id == 2:
			print "Discuter avec qui ?:"
			spk_id,spk_name = self.get_local_social()
			print "Vous parlez maintenant à:",spk_name
			RESADV = self.get_speak(spk_id+1,spk_name)
			if RESADV == -2:
				if rick['in'] == "XZ-321":
					if self.position == 'dans un coin':
						self.next_adv()
			if RESADV == -1:
				if rick['in'] == "XZ-321":
					if self.position == 'bar':
						print "Nouvelle position débloquée! :",BLOCKED[rick['in']]
						del BLOCKED[rick['in']]
			if RESADV == 0:
				print bcolors.OKBLUE+str("VOUS VENEZ DE COMMETTRE UN MEURTRE!")+bcolors.ENDC
			if RESADV == 1:
				print  bcolors.FAIL+str("VOUS VENEZ DE BLESSER QUELQU'UN!")+bcolors.ENDC
				
	def uplowPV(self):
		#simple non ?
		self.effect_object(1)

	def uplowEXP(self):
		REXP = 5
		self.rick_expup(REXP)

	def uphighPV(self):
		#si vous comprenez pas je peut rien faire pour vous ...
		self.effect_object(3)

	def death(self):
		print bcolors.WARNING+"VOUS ETES MORT !\nFIN DU JEU..."+bcolors.ENDC
		zzz=raw_inputs("")
		sys.exit(0)

	def effect_object(self,id_):
		print "VOUS UTILISEZ: ",objets[id_]['description']
		if id_ == 1:
			RPV = 10
			cur_pv,max_pv = rick['PV']['current'],rick['PV']['max']
			max_restore = max_pv-cur_pv
			if max_restore>RPV:
				restore_pv = RPV
			else:
				restore_pv = max_restore
			rick['PV']['current'] += restore_pv
			print "VOUS AVEZ RESTAURER ",bcolors.OKGREEN+str(restore_pv)+bcolors.ENDC, "PV !"
		if id_ == 2:
			RPV = 15
			cur_pv,max_pv = rick['PV']['current'],rick['PV']['max']
			max_restore = max_pv-cur_pv
			if max_restore>RPV:
				restore_pv = RPV
			else:
				restore_pv = max_restore
			rick['PV']['current'] += restore_pv
			print "VOUS AVEZ RESTAURER ",bcolors.OKGREEN+str(restore_pv)+bcolors.ENDC, "PV !"
		if id_ == 3:
			RPV = 35
			cur_pv,max_pv = rick['PV']['current'],rick['PV']['max']
			max_restore = max_pv-cur_pv
			if max_restore>RPV:
				restore_pv = RPV
			else:
				restore_pv = max_restore
			rick['PV']['current'] += restore_pv
			print "VOUS AVEZ RESTAURER ",bcolors.OKGREEN+str(restore_pv)+bcolors.ENDC, "PV !"
		if id_ == 4:
			REXP = 10
			self.rick_expup(REXP)

	def rick_expup(self,REXP):
		rick['EXP'],REXP = (rick['EXP']+(REXP)/rick['level']) % 100, (rick['EXP']+(REXP)/rick['level'])
		print "VOUS AVEZ ETES A ",str(REXP)+"%", "d'EXP !"
		if rick['EXP'] == 0:
			print bcolors.HEADER+str("VOUS AVEZ GAGNER UN NIVEAU D'EXP!")+bcolors.ENDC
			self.rick_levelup()

	def rick_levelup(self):
		random.seed(datetime.now())
		rick['level'] += 1
		ATK,DEF = random.randint(1,6),random.randint(1,4)
		print "VOUS GAGNEZ +"+bcolors.HEADER+str(ATK)+bcolors.ENDC+" ATK et "+bcolors.HEADER+str(DEF)+bcolors.ENDC+" DEF !"
		rick['atk']+=ATK
		rick['def']+=DEF

	def gain_exp(self,damage):
		return math.pow(math.log(damage*3+1),2)

	def get_speak(self,id_,name_):	
		print bcolors.BOLD+"VOUS COMMENCEZ UNE CONVERSATION AVEC:"+bcolors.ENDC,id_,name_
		while True:
			cur = dialogues[id_]['current']
			if cur in dialogues[id_]['dials'].keys():
				msg = dialogues[id_]['dials'][cur]['text']
				reps = dialogues[id_]['dials'][cur]['reponses']
				dialogues[id_]['dials'][cur]['reponses']["Rien a foutre, je me barre."] = "quit"
				rndreps =  list(reps.keys())

				#rndreps = random.shuffle(rkeys)
				print '[',name_,'] Dit:',msg
				rep_id,rep_name = self.action(rndreps,rnd=1)
				print "Vous lui dites:",rep_name
				if dialogues[id_]['dials'][cur]['reponses'][rep_name] == "quit":
					print bcolors.BOLD+"Vous quittez la conversation."+bcolors.ENDC
					return None


				if dialogues[id_]['dials'][cur]['reponses'][rep_name] == "good":
					if cur+1 != 0:
						if cur+1 in dialogues[id_]['dials'].keys():
							dialogues[id_]['current']+=1
						else:
							if -1 in dialogues[id_]['dials'].keys():
								dialogues[id_]['current'] = -1
								print bcolors.BOLD+"Vous quittez la conversation."+bcolors.ENDC
								return -1
							else:
								dialogues[id_]['current'] = -2
								print bcolors.BOLD+"Vous quittez la conversation.\nMission accomplie!"+bcolors.ENDC
								return -2
					else:
						if -1 in dialogues[id_]['dials'].keys():
							dialogues[id_]['current'] = -1
							print bcolors.BOLD+"Vous quittez la conversation."+bcolors.ENDC
							return -1
						if -2 in dialogues[id_]['dials'].keys():
							dialogues[id_]['current'] = -2
							print bcolors.BOLD+"Vous quittez la conversation.\nMission accomplie!"+bcolors.ENDC
							return -2

				if dialogues[id_]['dials'][cur]['reponses'][rep_name] == "bad":
					print '[',name_,'] Réponds:',dialogues[id_]['dials'][cur]['bad']

				if dialogues[id_]['dials'][cur]['reponses'][rep_name] == "quest":
					#ici le code de la quête
					for inv in personnages[id_]['inventaire'].keys():
						if inv in rick['inventaire'].keys():
							rick['inventaire'][inv] += personnages[id_]['inventaire'][inv]
						else:
							rick['inventaire'][inv] = personnages[id_]['inventaire'][inv]
						print "Vous recevez: +",bcolors.OKGREEN+str(personnages[id_]['inventaire'][inv])+bcolors.ENDC,bcolors.OKGREEN+objets[inv]['description']+bcolors.ENDC
					print '[',name_,'] Réponds: Au revoir...'
					lieux[rick['in']][self.position]['pnj'].remove(id_)
					del dialogues[id_]
					return -3
					
				if dialogues[id_]['dials'][cur]['reponses'][rep_name] == "kill":
					#ici le code d'échec %
					#affecter rick et le pnj
					print "VOUS ATTAQUEZ ",bcolors.BOLD+personnages[id_]['nom']+bcolors.ENDC
					dmgs = self.damage(rick,personnages[id_])
					personnages[id_]['PV']-=dmgs

					if personnages[id_]['PV']<=0:
						#si ça reussit
						lieux[rick['in']][self.position]['pnj'].remove(id_)
						del dialogues[id_]
						#gain exp
						self.rick_expup(self.gain_exp(dmgs))
						#ici le code de drop des gains
						
						return 0
					else:
						print "VOUS ETES ATTAQUEZ PAR",bcolors.BOLD+personnages[id_]['nom']+bcolors.ENDC
						rick['PV']['current']-=self.damage(personnages[id_],rick)
						return 1


	def damage(self,a,t):
		#ATK.a / 2 - DEF.t / 4 = degat infligé.
		#ATK.a : Attaque de l'attaquant
		#DEF.t : Defense de la cible
		damage = a['atk']/2.0 - t['def']/4.0
		print bcolors.WARNING+str(damage)+bcolors.ENDC+" DEGATS POUR "+bcolors.BOLD+t['nom']+bcolors.ENDC
		return damage

	def next_adv(self):
		print "ICI LE CODE DE LA SUITE"



	def start(self):
		if rick['in']=="XZ-321": 
			print INTRO+"\n"

		while True:
			print "\n\n\n\n"
			self.menu_root()



Jeu = Game()
Jeu.start()





