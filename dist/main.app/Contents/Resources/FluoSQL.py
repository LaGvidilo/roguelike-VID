# -*- coding: utf-8 -*-
import sys
reload(sys)
sys.setdefaultencoding("utf-8")
__version__ = "Zero"

import hashlib
import os

import random
def getpass(key,LEN=5,GRP=5):#parametre maitre LEN 8,GRP 5
	possibles = "ABCDEFXYZVHK0123456789"
	PASS = ""
	random.seed(key)
	for i in range(0,GRP):
		for j in range(0,LEN):
			PASS=PASS+random.choice(possibles)
		if i != GRP-1: PASS=PASS+'-'
	return PASS


#AUTOLOAD by LAGVIDILO
class autoload(object):
	def cherche(self,dir):
	    FichList = [ f for f in os.listdir(dir) if os.path.isfile(os.path.join(dir,f)) ]
	    return FichList

	def read_file(self,file):
	    f = open(file,"r")
	    R=f.read()
	    f.close()
	    return R

	def load_html(self,dir="pages"):
	    FL = self.cherche(dir)
	    R={}
	    for f in FL:
	        if f.split('.')[1]=="html":
	            BUFF = self.read_file(dir+"/"+f)
	            R[f.split('.')[0]] = BUFF
	    return R

	def load_tpl(self,dir="pages"):
	    FL = self.cherche(dir)
	    R={}
	    for f in FL:
	        if f.split('.')[1]=="tpl":
	            BUFF = self.read_file(dir+"/"+f)
	            R[f.split('.')[0]] = BUFF
	    return R

	def load_names_dict(self,dir):
		FL=self.cherche(dir)
		R=[]
		for f in FL:
			if f.split('.')[1]=="dict":
				R.append(dir+"/"+f)
				print "<<<",R,">>>"
		return R
"""
class Fluo(object):
	def __init__(self,cookies_username='auth_login',IP="localhost",port=8000):
		from bottle import route, run, template, get, post, request, response, static_file, redirect
		# AUTOLOAD PAR CYRIL COELHO
		autoload_ = autoload()
		self.pages = autoload_.load_html()
		self.tpl = autoload_.load_tpl()
		self.BUFF=""
		#self.pages={}
		self.FSQL=FluoSQL()
		self.IP,self.port = IP,port

	def buff_add(self,line):
		self.BUFF=self.BUFF+line+"\n"

	def make_simple_get(self):
		for key,i in self.pages.iteritems():
			self.buff_add("@get('/"+key+"')")
			self.buff_add("def "+key+"_html():")
			self.buff_add("	return '"+self.pages[key].replace("\n","")+"'")

		self.buff_add("run(host='"+self.IP+"', port="+str(self.port)+")")

	def init_buffer(self):
		self.BUFF=""

	def execute(self):
		from bottle import route, run, template, get, post, request, response, static_file, redirect

		#print "=====\n",self.BUFF,"\n====="
		exec(self.BUFF)
		self.BUFF = ""
 """
import ast
class FluoConfig(object):
	def __init__(self,name_file="main.config"):
		self.name_file = name_file
		self.data = self.load()
		if len(self.data)==0:
			self.data={}
			self.save(self.data)

	def save(self,data):
		f = open(self.name_file,'w')
		f.write(str(data))
		f.close()

	def load(self):
		try:
			f = open(self.name_file,'r')
			data = f.read()
			if len(data)>0:
				data = ast.literal_eval(data)
			f.close()
			return data
		except IOError:
			f = open(self.name_file,'w')
			f.write("{}")
			f.close()
			return {}

	def load_current_data(self):
		self.data = self.load()

	def get_all(self):
		self.load_current_data()
		return self.data

	def get_one(self,key):
		try:
			return self.data[key]
		except:
			return None

	def set_all(self,data):
		self.save(data)

	def set_one(self,key,value):
		self.data[key] = value
		self.save(self.data)
		

import httplib, urllib
class FluoSQL(object):
	def __init__(self,IP="128.79.156.73",user="phpmyadmin",password="6345",base="Exemple",http=False,secure={},phone="",conn_root="/CATIAERP/connect",req_root="/CATIAERP/req"):
		self.online = {'user':None, 'hash':None}
		self.conn_root,self.req_root = conn_root,req_root
		self.conn,self.cursor = None,None
		self.IP=IP
		self.username,self.passhash = user,password
		self.secure,self.phone=secure,phone
		self.headers = {"Content-type": "application/x-www-form-urlencoded","Accept": "text/plain"}
		if not http: 
			self.db_connect(IP,user,password,base)
		"""
		if signup:
			if http: self.http_connect(IP,user,hash_(password),secure,phone)
		else:	
			if http: self.http_connect(IP,user,hash_(password))
		"""
		self.__REQUEST__ = ""
		

	def db_connect(self,IP,user,password,base):#host="192.168.1.25",user="invite",password="1234", database="licorne"
		import mysql.connector
		self.conn = mysql.connector.connect(host=IP,user=user,password=password, database=base)#sqlite3.connect(link) #invite 1234
		self.conn.text_factory = str
		self.cursor = self.conn.cursor(buffered=True,dictionary=True)

	def db_close(self):
		self.conn.commit()
		self.conn.close()

	def http_connect(self,signup=False):
		phone=self.phone
		secure=self.secure
		IP=self.IP
		username,passhash=self.username,hash_(self.passhash) 
		
		if signup:
			params = urllib.urlencode({'@mode': 'new', '@username':username, '@hash':passhash, "@secure":secure, "@phone":phone})
		else:
			params = urllib.urlencode({'@mode': 'connect', '@username':username, '@hash':passhash})
		#print "ETAT: ",params	
		
		self.conn_http = httplib.HTTPConnection(self.IP)
		self.conn_http.connect()
		self.conn_http.request("POST", self.conn_root, params, self.headers)
		response = self.conn_http.getresponse()
		#print response.status, response.reason
		data = response.read()
		#print data
		return data	


	def send_request(self):
		#print "REQUEST SEND:",self.__REQUEST__
		params = urllib.urlencode({'@req': self.__REQUEST__, '@username':self.username, '@hash':hash_(self.passhash)})
		self.conn_http = httplib.HTTPConnection(self.IP)
		self.conn_http.connect()
		self.conn_http.request("POST", self.req_root, params, self.headers)
		response = self.conn_http.getresponse()
		#print response.status, response.reason
		data = response.read()
		#print data
		return data	


	def http_close(self):
		conn.close()

	def request_init(self):
		self.__REQUEST__ = ""

	def request_add(self,TMP):
		self.__REQUEST__=self.__REQUEST__+TMP

	def set_request(self,request,mode=0):
		self.request_init()
		self.__REQUEST__ = request
		type_request = self.__REQUEST__.split(' ')[0]
		self.cursor.execute(self.__REQUEST__)
		if type_request in "UPDATE INSERT DELETE": self.conn.commit()

		
		if type_request == "SELECT":
			RET = None
			if mode==1 or mode=="one":
				RET= self.cursor.fetchone()
			if mode==0 or mode=="all":
				RET= self.cursor.fetchall()
			if mode==-1:
				for row in self.cursor:
					RET = row[0]
					break

			return RET

	def get_reponse(self,mode=1):
		if "SELECT" in self.__REQUEST__:
			RET = None
			if mode==1 or mode=="one":
				RET= self.cursor.fetchone()
			if mode==0 or mode=="all":
				RET= self.cursor.fetchall()
			if mode==-1:
				for row in self.cursor:
					RET = row[0]
					break

			return RET

	def make_simple_request(self, type_request = "select", selected_row="*", values="", selected_from="Auth", WHERE={}, mode=0, set_current=False, inc=False, suparg=""):
		self.request_init()
		type_request=type_request.upper()
		if type_request=="DELETE":
			self.request_add("DELETE FROM ")
			self.request_add(selected_from+" ")
			for key in WHERE:
				try:
					K = str(int(WHERE[key]))
				except:
					K = '\"'+str(WHERE[key])+'\"'
				if "WHERE" not in self.__REQUEST__:
					if ("=" in key) or ("<" in key) or (">" in key):
						self.request_add("WHERE "+key+""+K+" ")
					else:
						self.request_add("WHERE "+key+"="+K+" ")
				else:
					if ("=" in key) or ("<" in key) or (">" in key):
						self.request_add("AND "+key+""+K+" ")
					else:
						self.request_add("AND "+key+"="+K+" ")



		if type_request=="INSERT":
			self.request_add("INSERT INTO ")
			self.request_add(selected_from+" (")
			if type(selected_row)!= list: selected_row=selected_row.split(',')
			if type(values)!= list: values=values.split(',')
			for i in range(0,len(selected_row)):
				self.request_add(selected_row[i])
				if i<len(selected_row)-1: self.request_add(", ")
			self.request_add(") VALUES(")
			for i in range(0,len(values)):
				#print "I:",i,"values[i]:",values[i]
				if (type(values[i]) == int) or (type(values[i]) == float):
					self.request_add(str(values[i]))
				else:
					self.request_add("\""+values[i]+"\"")
				if i<len(values)-1: self.request_add(", ")
			self.request_add(')')


		if type_request=="UPDATE":
			self.request_add("UPDATE ")
			self.request_add(selected_from+" SET "+selected_row+" ")
			if type(selected_row)!= list:selected_row=selected_row.split(',')
			if type(values)!= list:values=values.split(',')
			for i in range(0,len(selected_row)):
				if (type(values[i]) == int) or (type(values[i]) == float):
					self.request_add("="+str(values[i]))
				else:
					self.request_add("=\""+values[i]+"\"")

				if i<len(selected_row)-1: self.request_add(", ")
			self.request_add(" ")
			for key in WHERE:
				try:
					K = str(int(WHERE[key]))
				except:
					K = '\"'+str(WHERE[key])+'\"'
				if "WHERE" not in self.__REQUEST__:
					if ("=" in key) or ("<" in key) or (">" in key):
						self.request_add("WHERE "+key+""+K+" ")
					else:
						self.request_add("WHERE "+key+"="+K+" ")
				else:
					if ("=" in key) or ("<" in key) or (">" in key):
						self.request_add("AND "+key+""+K+" ")
					else:
						self.request_add("AND "+key+"="+K+" ")


		if type_request=="SELECT":
			self.request_add("SELECT ")
			self.request_add(selected_row+" ")
			self.request_add("FROM "+selected_from+" ")

			for key in WHERE:
				try:
					K = str(int(WHERE[key]))
				except:
					K = '\"'+str(WHERE[key])+'\"'
				if "WHERE" not in self.__REQUEST__:
					if ("=" in key) or ("<" in key) or (">" in key):
						self.request_add("WHERE "+key+""+K+" ")
					else:
						self.request_add("WHERE "+key+"="+K+" ")
				else:
					if ("=" in key) or ("<" in key) or (">" in key):
						self.request_add("AND "+key+""+K+" ")
					else:
						self.request_add("AND "+key+"="+K+" ")

		#print "self.__REQUEST__:",self.__REQUEST__

		if suparg!="":
			self.request_add(suparg)

		if type_request not in "INSERT UPDATE": print "\n\nSQL:" ,self.__REQUEST__,"\n\n"
		##print "EXECUTE:","self.cursor.execute(self.__REQUEST__)"
		if not set_current:
			self.cursor.execute(self.__REQUEST__)
			if type_request in "UPDATE INSERT DELETE": self.conn.commit()
			RET = True
			self.request_init()
			if type_request == "SELECT":
				
				if mode==1 or mode=="one":
					RET= self.cursor.fetchone()
				if mode==0 or mode=="all":
					RET= self.cursor.fetchall()
				if mode==-1:
					for row in self.cursor:
						RET = row[0]
						break
			return RET
		else:
			if mode!=-1:
				return self.__REQUEST__


import hashlib
def hash_(passwd):
	passh = hashlib.sha224(passwd).hexdigest()
	#print "1-",passwd,"-";passh
	return passh
		
import random
import numpy
import base64
import os
import ast
import zlib
class CID(object):#Cryptage Inter-Dimensionnel
	def __init__(self,size=16):#Data : 8 == 4096 Bits
		self.size = size - 1 #Test $PRL$
		self.multiverse = numpy.zeros((size,size,size,size))
		self.buffer_size = 0

	def encode(self,data,key):
		self.buffer_size = 0
		random.seed(hash_(key))
		for i in data:
			x,y,z,d = random.randint(0,self.size),random.randint(0,self.size),random.randint(0,self.size),random.randint(0,self.size)
			self.multiverse[x][y][z][d] = ord(i)
			self.buffer_size += 1

	def get_matrix(self):
		return self.multiverse

	def decode(self,key):
		OUT = ""
		random.seed(hash_(key))
		for j in range(0,self.buffer_size):
			x,y,z,d = random.randint(0,self.size),random.randint(0,self.size),random.randint(0,self.size),random.randint(0,self.size)
			buff = int(self.multiverse[x][y][z][d])
			OUT = OUT +str(chr(int(buff)))
		return OUT

	def get_data(self):
		DATA = []
		t1,t2,t3,t4 = 0,0,0,0
		for i1 in self.multiverse:
			t2=0
			for i2 in i1:
				t3=0
				for i3 in i2:
					t4=0
					for i4 in i3:
						if int(i4)!=0: DATA.append({'pos':(t1,t2,t3,t4),'data':i4})
						t4+=1
					t3+=1
				t2+=1
			t1+=1
		return DATA

	def set_data(self,DATA):
		self.multiverse = numpy.zeros((self.size+1,self.size+1,self.size+1,self.size+1))
		o=0
		for i in DATA:
			t1,t2,t3,t4 = i['pos']
			self.multiverse[t1][t2][t3][t4] = i['data']
			o+=1
		self.buffer_size = o

	def load(self,fichier='test.CID'):
		f = open(fichier,'r+b')
		buff = f.read()
		f.close()
		buff = zlib.decompress(buff)
		buff = ast.literal_eval(base64.b64decode(buff))
		self.size, self.buffer_size = buff['size'], buff['buffer_size']		
		DATA = ast.literal_eval(base64.b64decode(buff['data']))
		self.set_data(DATA)

	def save(self,fichier="test.CID"):
		DATA = base64.b64encode(str(self.get_data()))
		DICTBUFF = base64.b64encode(str({'data':DATA,'buffer_size':self.buffer_size,'size':self.size}))
		print "Normal len of buffer:",len(DICTBUFF)
		DICTBUFF = zlib.compress(DICTBUFF,9)
		print "After zlib len of buffer:",len(DICTBUFF)
		f = open(fichier,'w+b')
		f.write(DICTBUFF)
		f.close()

	def get_data_save(self):
		DATA = base64.b64encode(str(self.get_data()))
		DICTBUFF = base64.b64encode(str({'data':DATA,'buffer_size':self.buffer_size,'size':self.size}))
		print "Normal len of buffer:",len(DICTBUFF)
		DICTBUFF = zlib.compress(DICTBUFF,9)
		print "After zlib len of buffer:",len(DICTBUFF)

		return DICTBUFF

def CID_encrypt(data,key,fichier='test.CID',size=8):
	__CID__ = CID(size)
	__CID__.encode(data,key)
	__CID__.save(fichier)

def CID_decrypt(key,fichier='test.CID'):
	__CID__ = CID()
	__CID__.load(fichier)
	return __CID__.decode(key)

"""
import rsa
def get_keys(bits=1024):
	pubkey, privkey = rsa.newkeys(bits,poolsize=2)
	pubkey = int(str(pubkey).split('PublicKey(')[1].split(',')[0])
	privkey = int(str(privkey).split('PrivateKey(')[1].split(',')[0])
	return pubkey, privkey
"""
import mmap
import datetime
def shred_file(fichier,passes=30):
	random.seed(datetime.datetime.now())
	with open(fichier,'r+b') as f:
		mm = mmap.mmap(f.fileno(),0)
		for e in range(0,passes):
			mm.seek(0)
			for n in range(0,mm.size()):
				byte = mm.read_byte()
				mm.seek(0+n)
				mm.write_byte(chr(random.randint(0,255)))
		mm.close()
	os.remove(fichier)
"""
from captcha.image import ImageCaptcha
class captcha(object):
	def __init__(self):
		random.seed(datetime.datetime.now())

	def make(self):
		caracts="AZEQSDWXCRTYUIOPFGHJKLMVBN0123456789"
		OUT = []
		for i in range(0,50):
			buff = ""
			for j in range(0,5):
				buff=buff+random.choice(caracts)

			img = ImageCaptcha(fonts=["KICOMIC_.TTF"])
			data = img.generate(buff)
			data.seek(0)
			b64data = base64.b64encode(data.read())

			OUT.append({'id':i,'text':buff,'image':b64data})
		return OUT

	def make_file(self):
		caracts="AZEQSDWXCRTYUIOPFGHJKLMVBN0123456789"
		OUT = []
		for i in range(0,100):
			buff = ""
			for j in range(0,8):
				buff=buff+random.choice(caracts)

			img = ImageCaptcha(fonts=["KICOMIC_.TTF"])
			data = img.generate(buff)
			RND_=random.randint(0,10000000)
			file_="captcha/"+hash_(""+str(RND_))+'.png'
			link_file = "static/captcha/"+hash_(""+str(RND_))+'.png'
			img.write(buff,file_)

			OUT.append({'id':i,'text':buff,'file':file_,'link':link_file})
		return OUT
"""

class links_selector(object):
	def __init__(self):
		self.links = {}
		self.ids = {}
		self.artids = {}

	def getall(self):
		return {'links':self.links,'ids':self.ids,'artnames':self.artids}

	def addart(self,categorie,article,id_art):
		self.links[categorie].append({'nom':article,'id':id_art})
		self.artids[id_art] = article

	def addcat(self,categorie,id_cat):
		if categorie not in self.links.keys():
			self.links[categorie] = []
			self.ids[categorie] = id_cat



import smtplib
from email.mime.multipart import MIMEMultipart
from email.mime.text import MIMEText
#from bottle import template#For the HTML/CSS body's of mail
from email.MIMEImage import MIMEImage
#html = """\
#<html>
#  <head></head>
#  <body>
#    <p>Hi!<br>
#       How are you?<br>
#       Here is the <a href="https://www.python.org">link</a> you wanted.
#    </p>
#  </body>
#</html>
#"""

class notif_mail(object):
	def __init__(self,title,message,link,textlink,directory="mailing"):
		self.title,self.message,self.link,self.textlink = title,message,link,textlink
		self.directory = directory
	def get_code(self):
		code = template(self.directory+'/notif.tpl',titre=self.title,message=self.message,lien=self.link,texte_lien=self.textlink)
		return unicode(code).encode('utf-8').strip()

class promo_mail(object):
	def __init__(self,title,message,cartpromo,shippromo,codepromo,directory="mailing"):
		self.title,self.message,self.cartpromo,self.shippromo = title,message,cartpromo,shippromo
		self.codepromo = codepromo
		self.directory = directory
	def get_code(self):
		code = template(self.directory+'/promo.tpl',titre=self.title,message=self.message,
			cartpromo=self.cartpromo,shippromo=self.shippromo,codepromo=self.codepromo)
		return unicode(code).encode('utf-8').strip()

class pub_mail(object):
	def __init__(self,title,message,lien,image,directory="mailing"):
		self.title,self.message = title,message
		self.directory = directory
		self.lien = lien
		self.image = image
	def get_code(self):
		code = template(self.directory+'/pub.tpl',titre=self.title,message=self.message,lien=self.lien,image=self.image)
		return unicode(code).encode('utf-8').strip()

class mailing(object):
	def __init__(self,login,password,image=None):
		self.me, self.mepass = login, password

		self.image = image

	def set_destination(self,list_dest):
		self.you = list_dest

	def set_message(self,subject,message):
		self.html = message
		self.subject = subject

	def send(self):
		for you_current in self.you:
			msg = MIMEMultipart('alternative')
			msg['Subject'] = self.subject
			msg['From'] = self.me
			msg['To'] = you_current

			part = MIMEText(self.html, 'html')

			msg.attach(part)

			fp = open('mailing/logomail.png', 'rb')
			Image = MIMEImage(fp.read())
			fp.close()
			Image.add_header('Content-ID', '<logo>')
			msg.attach(Image)

			if self.image!=None:
				#print self.image[0]
				#print "IMAGE:",len(self.image)
				Photo = MIMEImage(self.image)
				Photo.add_header('Content-ID', '<image>')
				msg.attach(Photo)

			s = smtplib.SMTP('smtp.gmail.com', 587)

			s.ehlo()
			s.starttls()
			s.ehlo()
			s.login(self.me, self.mepass)
			s.sendmail(self.me, you_current, msg.as_string())
			s.quit()
			print "Mail envoyer à:", you_current," au propos de:",self.subject

class validator_email(object):
	def new(self,email,seed):
		random.seed(email+str(seed))
		return hashlib.sha512(str(random.randint(0,10**10))).hexdigest()
	def verif(self,token,email,seed):
		if self.new(email,seed) == token:
			return True
		else:
			return False					
"""
# DATA INPUTS: email, (datetime)
# SET
Histo = FluoConfig("validations.config")
SEED = str(datetime.datetime.now())
Histo.set_one(email,{'seed':SEED})

# TOKEN
token = validator_email().new(email,SEED)

# DATA OUTPUTS: token, email
# ENVOI DU token DANS UN EMAIL PAR LIEN DANS UNE ROUTE

#============
# DATA INPUTS: email, (SEED), (datetime)
# GET
Histo = FluoConfig("validations.config")
SEED = Histo.get_one(email)['seed'] # {'seed': x}

# & VERIF
if validator_email().verif(email,SEED):
	return True
else:
	SEED = str(datetime.datetime.now())
	Histo.set_one(email,{'seed':SEED})

	# TOKEN
	token = validator_email().new(email,SEED)

	# DATA OUTPUTS: token, email
	# ENVOI DU token DANS UN EMAIL PAR LIEN DANS UNE ROUTE
	return False
"""
class confirm_email(object):
	def set(self,email):
		# DATA INPUTS: email, (datetime)
		# SET
		Histo = FluoConfig("validations.config")
		SEED = str(datetime.datetime.now())
		Histo.set_one(email,{'seed':SEED})

		# TOKEN
		token = validator_email().new(email,SEED)

		# DATA OUTPUTS: {'token': token, 'email': email}
		return {'token': token, 'email': email}

	def get(self,token_,email):
		# DATA INPUTS: email, (SEED), (datetime)
		# GET
		Histo = FluoConfig("validations.config")
		SEED = Histo.get_one(email)['seed'] # {'seed': x}

		# & VERIF
		if validator_email().verif(token_,email,SEED):
			return {'token': True, 'email': email}
		else:
			SEED = str(datetime.datetime.now())
			Histo.set_one(email,{'seed':SEED})

			# TOKEN
			token = validator_email().new(email,SEED)

			# DATA OUTPUTS: token, email
			# ENVOI DU token DANS UN EMAIL PAR LIEN DANS UNE ROUTE
			return {'token': token, 'email': email}
"""
# sign
confirm = confirm_email().set(email)
lien = confirm['token']+"/"+confirm['email']

# ENVOI DU MAIL

#==========
# valid_email
confirm = confirm_email().get(token,email)
if confirm['token'] == True:
	#Confirmation du compte (email)
else:
	lien = confirm['token']+"/"+confirm['email']	
	# ENVOI DU MAIL


"""
#from overload import *
#from whatever import _
import ast
import random
import datetime
class Traceur(object):
	class Historique(object):
		def __init__(self,name_file="historiques.$"):
			self.name_file = name_file
			self.data = self.load()
			if len(self.data)==0:
				self.data={}
				self.save(self.data)

		def save(self,data):
			f = open(self.name_file,'w')
			f.write(str(data))
			f.close()

		def save_all(self):
			f = open(self.name_file,'w')
			f.write(str(self.data))
			f.close()

		def load(self):
			try:
				f = open(self.name_file,'r')
				data = f.read()
				if len(data)>0:
					data = ast.literal_eval(data)
				f.close()
				return data
			except IOError:
				f = open(self.name_file,'w')
				f.write("{}")
				f.close()
				return {}

		def load_current_data(self):
			self.data = self.load()

		def get_all(self):
			self.load_current_data()
			return self.data

		#@overload(callable,(int,str))
		def get_one(self,key):
			try:
				return self.data[key]
			except:
				return None

		#@overload(callable,(int,str),int)
		def get_one(self,key,id_):
			try:
				return self.data[key][id_]
			except:
				return None

		#@overload(callable,(dict,str))
		def set_all(self,data):
			self.save(data)

		#@overload(callable,(int,str),(int,dict,list,str))
		def set_one(self,key,value):
			self.data[key] = value
			self.save(self.data)

		#@overload(callable,(int,str),int,(int,dict,list,str))
		def set_one(self,key,id_,value):
			self.data[key][id_] = value
			self.save(self.data)

		def create_key(self,key):
			if key not in self.data.keys():
				self.data[key]={}


		def create_id(self,key,id_):
			self.create_key(key)
			if id_ not in self.data[key].keys(): self.data[key][id_] = []
			self.data[key][id_].append([0,None,None])

		def set_like(self,key,id_,like=1):
			self.data[key][id_][-1][1] = like

		def set_like_protected(self,key,id_,like=1):
			if self.data[key][id_][-1][1]==None: self.data[key][id_][-1][1] = like


		def set_cart(self,key,id_,cart=1):
			self.data[key][id_][-1][0] = cart

		def set_timestamp(self,key,id_):
			self.data[key][id_][-1][2] = str(datetime.datetime.now()).split('.')[0]


		def timeline(self,key):
			timeline = {}
			for i in self.data[key].keys():
				for j in self.data[key][i]:
					cart,like,timestamp = j[0],j[1],j[2]
					if timestamp not in timeline.keys(): timeline[timestamp] = [i,cart,like]
			line = []
			for i in timeline.keys():
				temps = datetime.datetime.strptime(i,'%Y-%m-%d %H:%M:%S')
				line.append(temps)

			line = sorted(line)

			return timeline,line

		def classifier(self,key):
			secondaire,compatible,ultime = [],[],[]
			for i in self.data[key].keys():
				if ((self.data[key][i][-1][0]) and (self.data[key][i][-1][1])): ultime.append(i)
				if (((self.data[key][i][-1][0]==1) and (self.data[key][i][-1][1]==0))) or (None == self.data[key][i][-1][1]): compatible.append(i)
				if ((self.data[key][i][-1][0]==0) and (self.data[key][i][-1][1]==1)): secondaire.append(i)
			return {'secondaire':secondaire,'compatible':compatible,'ultime':ultime}


	def __init__(self,fichier="tracedata/historique.$"):
		self.pile = self.Historique(fichier)

	def set_histo(self,user,id_article,type_="cart"):
		self.pile.create_id(user,id_article)
		if type_ == "unknow":
			self.pile.set_like_protected(user,id_article,None) 
			self.pile.set_timestamp(user,id_article)
		if type_ == "like":
			self.pile.set_like(user,id_article,1) 
			self.pile.set_timestamp(user,id_article)
		if type_ == "cart":
			self.pile.set_cart(user,id_article,1)
			self.pile.set_timestamp(user,id_article)
		self.pile.save_all()

	def get_classified_histo(self,user):
		return self.pile.classifier(user)

	def get_list_user(self):
		return self.pile.data.keys()

	def get_timeline(self,user):
		return self.pile.timeline(user)
"""
TRC = FluoSQL.Traceur()
TRC.set_histo(user,id_art,cart/like/unknow)
TRC.get_classified_histo(user)
"""






#Modules de sessions
"""
from bottle import request
import geocoder
def get_ip():
	return request.environ.get('REMOTE_ADDR')
"""

import datetime
from threading import Timer
class _session_(object):
	class sessions(object):
		def __init__(self,name_file="tracedata/session.$"):
			self.name_file = name_file
			self.data = self.load()
			if len(self.data)==0:
				self.data={}
				self.save(self.data)
			self.connections = {}
			for i in self.data.keys():
				self.set_connection(i)

		def save(self,data):
			f = open(self.name_file,'w')
			f.write(str(data))
			f.close()

		def load(self):
			try:
				f = open(self.name_file,'r')
				data = f.read()
				if len(data)>0:
					data = ast.literal_eval(data)
				f.close()
				return data
			except IOError:
				f = open(self.name_file,'w')
				f.write("{}")
				f.close()
				return {}

		def set_connection(self,key,timeout=600):
			self.connections[key] = Timer(timeout,self.kill_connection,args=[key])
			self.connections[key].start()
			print "[RESEAU]",self.data[key]," Viens de se connecter !"

		def kill_connection(self,*args):
			key = args[0]
			try:
				print "[RESEAU]","Fin de session pour: ",self.data[key]
				del self.connections[key]
				del self.data[key]
				self.save(self.data)
			except:
				print "[RESEAU]","Erreur de session: ",key,"(certainement un premier appel de fonction)"
			return "ERROR"

		def load_current_data(self):
			self.data = self.load()

		def get_all(self):
			self.load_current_data()
			return self.data

		def get_one(self,key):
			try:
				return self.data[key]
			except:
				return None

		def set_all(self,data):
			self.save(data)

		def set_one(self,key,value):
			self.data[key] = value
			self.save(self.data)

		def connect(self,username):
			TMP =  str(datetime.datetime.now())+"--"+str(random.randint(0,100))+"--"+str(get_ip())
			for k,u in self.data.items():
				if u == username:
					del self.data[k]
					self.kill_connection(k)
			self.set_one(TMP,username)
			self.set_connection(TMP)

		def is_connected(self,username):
			return username in self.data.values()

		def disconnect(self,username):
			for k,u in self.data.items():
				if u == username:
					del self.data[k]
					self.kill_connection(k)
			self.save(self.data)

	def __init__(self):
		self.__SESSIONS__ = self.sessions()


	def action(self,username,mode="connect"):
		if mode == "connect": self.__SESSIONS__.connect(username)
		if mode == "verify": return self.__SESSIONS__.is_connected(username)
		if mode == "disconnect": return self.__SESSIONS__.disconnect(username)

"""
Pour trouver un nombre dans un LONG INT aléatoire
def test(l=100000,f="6345",seed=7):
	random.seed(seed)
	for i in range(0,l):
		print i,'/',l
		a=str(random.randrange(0,1000000*(10**256)))
		if f in a:
			print "FIND IN POS:",a.index(f),"OF GENERATION:",i
			return (a.index(f),i)

"""