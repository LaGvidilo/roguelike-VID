#coding: utf-8
from __future__ import unicode_literals
"""
python -m pip install terminaltables

"""
import sys
reload(sys)
sys.setdefaultencoding("utf-8")

#Pour les effets des drogues et autres substances
from threading import Timer
#Timer(timeout,self.kill_connection,args=[key])

#pour afficher des tableaux(ex: l'inventaire)
from terminaltables import AsciiTable
import os
import platform
def Clean():
    if platform.system() == "Windows":
        os.system("cls")
    else:
        os.system("clear")

#import pygame
#pygame.mixer.init()
"""
class AudioMotor(object):
	class Music(object):
		def LoadPlay(self,filepath):
			pygame.mixer.music.load(filepath)
			pygame.mixer.music.play()
		def Stop(self):
			pygame.mixer.music.stop()

AUDIO = AudioMotor()
"""

SANG = []
SANG_TIMERS = []

def cheat_pfc(choix):
	if choix == 0:
		return 1
	if choix == 1:
		return 2
	if choix == 2:
		return 0

def sample(p):
	if random.random() < p:
		return 1
	else:
		return 0

import ast
def jsonKeys2str(x):
	if isinstance(x, dict):
		return {int(k):v for k,v in x.items()}
	return x
def loader(filename):
	try:
		f = open("gamedata/"+filename,'r+b')
		DATA = f.read()
		f.close()
		DATA = DATA.replace("null","None").replace("true","True").replace("false","False").decode('ISO-8859-2')
		DATA = ast.literal_eval(DATA)
	except:
		f = open(filename,'r+b')
		DATA = f.read()
		f.close()
		DATA = DATA.replace("null","None").replace("true","True").replace("false","False").decode('ISO-8859-2')
		DATA = ast.literal_eval(DATA)

	try:
		DATA = jsonKeys2str(DATA)
	except:
		pass

	DICTIONNAIRE = dict(DATA)
	for i in DICTIONNAIRE.keys():
		try:
			if "inventaire" in DICTIONNAIRE[i].keys():
				#print DICTIONNAIRE[i].keys()
				#print DICTIONNAIRE[i]['inventaire']
				DICTIONNAIRE[i]['inventaire'] = jsonKeys2str(DICTIONNAIRE[i]['inventaire'])
		except:
			pass

		try:
			if "dials" in DICTIONNAIRE[i].keys():
				#print DICTIONNAIRE[i].keys()
				#print DICTIONNAIRE[i]['dials']
				DICTIONNAIRE[i]['dials'] = jsonKeys2str(DICTIONNAIRE[i]['dials'])
		except:
			pass


	return DICTIONNAIRE

def read_file(fichier):
	f = open(fichier,'r+b')
	R=f.read()
	f.close()
	return R

crafts = loader("craft.dict")
objets = loader("items.dict")
def craft(objet1=None,objet2=None,objet3=None):
	if objet3 == None or objet3=="":
		objets_ = sorted([objet1,objet2])
	else:
		objets_ = sorted([objet1,objet2,objet3])
	for i in crafts.keys():
		if sorted(crafts[i]) == objets_:
			print "Vous fabriquer: ",objets[i]['description']
			for j in objets_: 
				rick['inventaire'][j] -= 1
				if rick['inventaire'][j] < 1: del rick['inventaire'][j]
			if i not in rick['inventaire'].keys(): 
				rick['inventaire'][i] = 1 
			else:
				rick['inventaire'][j] += 1
			break
			

dialogues = loader("dialogues.dict")
lieux = loader("lieux.dict")
personnages = loader("personnages.dict")
rick = loader("rick.dict")
generic = loader("generic.dict")


BLOCKED = {'XZ-321':'dans un coin'}
DIMBLOCKED = ["D-634","F-352","SX-2","XY-422"]

INTRO="""
Vous venez d'arriver dans la dimension XZ-321 en passant par un trou de vers a bord de votre vaisseau.\n
Votre mission et de retrouver votre pisto-portail, ramener Jerry sur la terre de D-634.\n
Votre vaisseaux rentrera sur D-634 seul grace au pilotage automatique.
""".replace('/t','')

class bcolors:
	HEADER = '\033[95m'
	OKBLUE = '\033[94m'
	OKGREEN = '\033[92m'
	WARNING = '\033[93m'
	FAIL = '\033[91m'
	ENDC = '\033[0m'
	BOLD = '\033[1m'
	UNDERLINE = '\033[4m'


GROUP = rick['group']



import key
clavier = key.key()


import random
from datetime import datetime
import math
#import os
#os.system('cls||clear')
class Game(object):
	def __init__(self):
		self.position = None


	def damage(self,a,t):
		#ATK.a / 2 - DEF.t / 4 = degat infligé.
		#ATK.a : Attaque de l'attaquant
		#DEF.t : Defense de la cible
		damage = a['atk']/2.0 - t['def']/4.0
		if damage > 0:
			print bcolors.WARNING+str(damage)+bcolors.ENDC+" DEGATS POUR "+bcolors.BOLD+t['nom']+bcolors.ENDC
			return damage
		else:
			print "AUCUNS DEGATS..."
			return 0


	def next_adv(self):
		#print "ICI LE CODE DE LA SUITE"
		pass


	def start(self):
		if rick['in']=="XZ-321": 
			print INTRO+"\n"

		while True:
			print "\n\n\n\n"
			self.menu_root()

	def menuselector(self,options=["OPTION 1","OPTION 2","ACTION","ANNULER"],title="PISTO-PORTAIL VERS:"):
		K = 0
		acts = options
		while True:
			Clean()
			print title
			ll = 0
			DIMS_=""
			for i in acts:
				if acts.index(i) == K:
					DIMS_ = DIMS_ + bcolors.HEADER+i+bcolors.ENDC+"   "
				else:
					DIMS_ = DIMS_ + i +"   "
				ll = (ll + 1) % 8
				if ll ==0:
					DIMS_=DIMS_+"\n"

			print DIMS_

			TOUCHE = clavier.getkey()
			if TOUCHE == "down": K+=8
			if TOUCHE == "up": K-=8
			if TOUCHE == "left": K+=1
			if TOUCHE == "right": K-=1

			if K < 0: K = 0
			if K > len(acts)-1: K = len(acts)-1 

			if (TOUCHE == "start") or (TOUCHE == "select"): 
				return (K,acts[K])


	def action(self,choix,rnd=0):
		if rnd:
			random.shuffle(choix)
		while True:
			for i in choix:
				print choix.index(i),"-",i

			c = raw_input("Votre choix: ")
			try:
				c=int(c)
			except:
				c=-3
			if  -1 < c <= len(choix):
				return (c,choix[c])

	def action_conv(self,choix,rnd=0):
		ROT = random.choice(choix)
		if rnd:
			random.shuffle(choix)
		while True:
			for i in choix:
				if ROT == i:
					print choix.index(i),"-",i + " Buuaarrp"
				else:
					print choix.index(i),"-",i

			c = raw_input("Votre choix: ")
			try:
				c=int(c)
			except:
				c=-3
			if  -1 < c <= len(choix):
				return (c,choix[c])


	def social(self,choix):
		while True:
			for i in choix:
				print choix.index(i),"-",personnages[i]['nom']

			c = raw_input("Parler à: ")
			try:
				c=int(c)
			except:
				c=-3
			if  -1 < c <= len(choix):
				return (choix[c]-1,personnages[choix[c]]['nom'])

	def get_local_move(self):
		try:
			BLCKD = BLOCKED[rick['in']]
		except:
			BLCKD = None 

		ACT = []
		for i in lieux[rick['in']].keys():
			if i != BLCKD:
				ACT.append(i)

		return self.action(ACT)


	def get_dim_move(self):
		try:
			BLCKD = DIMBLOCKED
		except:
			BLCKD = None 

		ACT = []
		for i in lieux.keys():
			if i != BLCKD:
				ACT.append(i)

		return self.menuselector(ACT)


	def get_local_social(self):
		PERSONS = lieux[rick['in']][self.position]['pnj']
		for i in GROUP:
			if i not in PERSONS:
				PERSONS.append(i)
		if self.position!=None: return self.social(PERSONS)


	def television_info(self,chx_name):
		if chx_name == 'Regarder la chaine infos':
			if rick['in'] == 'XZ-321':
				print """
					Programme TV* Grazzoook !!! grazooookkk !!! Bienvenu dans ce programme, aujourd'hui grande catastrophe en vus, plusieurs planètes ont été effacé de multiples univers et nous n'avons en ce moment aucune piste indiquant qui serait ce voleur de planète.
					Flash culture la célèbre chanteuse Shaki-swift sortira bientôt son prochain tube et elle annonce pour son prochain clip qu'elle compte tourner un clip interdimensionnel qu'elle commencera depuis la dimesion XY-422 car elle s'est récemment procuré un objet appelé Pistolet-portail...
					""".replace('\t','')

	def dealer(self,x):
		self.add_to_inv(x)

	def search(self):
		print "VOUS CHERCHER QUELQUE CHOSE D'INTERESSANT..."
		chance = sample(0.01)
		objs = [1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21,22]
		if chance:
			obj = random.choice(objs)
			print "VOUS TROUVEZ:",objets[obj]['description']
			self.add_to_inv(obj)
		if not chance:
			print "VOUS NE TROUVEZ RIEN."

	def add_to_inv(self,x):
		if x in rick['inventaire'].keys():
			rick['inventaire'][x] = rick['inventaire'][x] + 1
		else:
			rick['inventaire'][x] = 1

	def sub_to_inv(self,x):
		if x in rick['inventaire'].keys():
			if rick['inventaire'][x]>1:
				rick['inventaire'][x] = rick['inventaire'][x] - 1
			else:
				del rick['inventaire'][x]

	def scan(self):
		print "VOUS UTILISER UN SCAN..."
		if rick['in'] == "XY-422":
			print "LE SCAN INDIQUE: ", "RIEN DE SPECIAL DETECTE."

	def echange(self):
		objech1 = random.randint(1,22)
		objech2 = random.randint(1,22)
		print "Le preteur sur gage veux: ",objets[objech1]['description'], "en echange de: ",objets[objech2]['description']
		if objech1 in rick['inventaire']:
			print "Voulez vous faire l'échange?:"
			chx_id,chx_name = self.action(["non","oui"])
			if chx_id == 1:
				if chx_name == "oui":
					rick['inventaire'][objech1] -= 1
					if rick['inventaire'][objech1] <= 0: del rick['inventaire'][objech1]
					self.add_to_inv(objech2)
					print "VOUS AVEZ CONCLU UNE AFFAIRE."
		else:
			print "VOUS N'AVEZ PAS DE: ",objets[objech1]['description']

	def sell(self):
		avendre = {}
		for i in rick['inventaire'].keys():
			print "Vous pouvez vendre: ",objets[i]['description'],"POUR:",objets[i]['price'],"schmeckles." 
			avendre[i] = {'prix':objets[i]['price'],'stock':rick['inventaire'][i]}
		acts = []
		recup_obj = {}
		for i in sorted(avendre.keys()):
			acts.append(objets[i]['description'])
			recup_obj[objets[i]['description']] = i
		acts.append("Annuler")
		chx_id,chx_name = self.action(acts)
		
		if chx_name != "Annuler":
			obj = recup_obj[chx_name]
			print "En vendre combien?:"
			acts = []
			for j in range(0,avendre[obj]['stock']+1):
				acts.append("x"+str(j)) 
			chx_id,chx_name = self.action(acts)
			if chx_id == 0:
				print "Vous changez d'avis et ne vendez pas."
			if chx_id > 0:
				rick['inventaire'][obj] -= chx_id
				print "VOUS VENDEZ x"+str(chx_id),"-",objets[obj]['description']
				rick['schmeckles'] += objets[obj]['price']
				print "VOUS GAGNEZ:",objets[obj]['price'],"schmeckles".upper()
				if rick['inventaire'][obj] <= 0 : del rick['inventaire'][obj]
		else:
			print "Vous ne vendez rien finalement."

	def menu_root(self):
		gnc = None
		if rick['PV']['current']<=0: self.death()
		if len(GROUP)>0:
			for i in GROUP:
				print "VOUS ETES SUIVIS PAR: ",personnages[i]['nom']

		print "VOUS ETES DANS LA DIMENSION: ", bcolors.BOLD+str(rick['in'])+bcolors.ENDC
		if self.position!=None: print "VOUS ETES A LA POSITION:",bcolors.BOLD+str(self.position)+bcolors.ENDC
		print "VOUS POUVEZ VOUS DEPLACER."
		print "VOUS POUVEZ UTILISER VOTRE INVENTAIRE."
		if rick['PV']['current']<rick['PV']['max']/2.0:
			print "VOUS AVEZ",bcolors.WARNING+str(rick['PV']['current'])+bcolors.ENDC,"/",rick['PV']['max'],"PV"#'PV': {'current':19,'max':25}
		else:
			print "VOUS AVEZ",rick['PV']['current'],"/",rick['PV']['max'],"PV"#'PV': {'current':19,'max':25}
	
		print "VOUS ETES NIVEAU:",bcolors.HEADER+str(rick['level'])+bcolors.ENDC
		print "VOUS AVEZ:",rick['schmeckles'], "SCHMECKLES."

		if self.position!=None: 
			if (len(lieux[rick['in']][self.position]['pnj'])>0) or (len(GROUP)>0): 
				print "VOUS POUVEZ DISCUTER."
				actions_list = ["Se deplacer...","Voir l'inventaire...","Discuter..."]
			else:
				actions_list = ["Se deplacer...","Voir l'inventaire..."]
			if lieux[rick['in']][self.position]['generic']!= -1:
				gnc = generic[lieux[rick['in']][self.position]['generic']]
				for i in gnc.keys():
					actions_list.append(i)

		else:
			actions_list = ["Se deplacer...","Voir l'inventaire..."]

		if rick['pisto-portail']:
			actions_list.append("Pisto-portail...")

		chx_id,chx_name = self.action(actions_list)

		self.television_info(chx_name)

		if gnc != None:
			if chx_id > 1 and chx_name != "Pisto-portail..." and chx_name != "Discuter...":
				#print "<<INFO>>:",chx_id,chx_name
				lgen = lieux[rick['in']][self.position]['generic']
				#print "generic=",generic
				generic_actarg = 'self.'+generic[lgen][chx_name]['func']+'()'

				if "dealer" == generic[lgen][chx_name]['func']:
					generic_actarg = 'self.'+generic[lgen][chx_name]['func']+'('+str(generic[lgen][chx_name]['id'])+')'
				price = generic[lgen][chx_name]['price']
				money = generic[lgen][chx_name]['money']
				if money == "PV":
					if rick[money]['current']>price:
						print "VOUS PAYEZ: ",price,money
						rick[money]['current']-=price
						exec(generic_actarg)
					else:
						print "VOUS N'AVEZ PAS ASSEZ DE: ",money
				if money == "None":
					exec(generic_actarg)
				else:
					if rick[money]>=price:
						print "VOUS PAYEZ: ",price,money
						rick[money]-=price
						exec(generic_actarg)
					else:
						print "VOUS N'AVEZ PAS ASSEZ DE: ",money




		if chx_id == 0:
			print "Voici les lieux possibles:"
			mve_id,mve_name = self.get_local_move()
			print "Vous êtes maintenant à:",bcolors.BOLD+str(mve_name)+bcolors.ENDC
			self.position = mve_name


		if chx_id == 1:
			if len(rick['inventaire'].keys())>0:
				print "Voici votre inventaire:"
				RINV = {}
				table_inv = [["CONSOMMABLE","UTILISABLE"]]
				usable_inv = []
				consommable_inv = []
				for inv in rick['inventaire'].keys():
					if objets[inv]['consommable']:
						consommable_inv.append("(x"+str(rick['inventaire'][inv])+")"+str(objets[inv]['description']))
						RINV[objets[inv]['description']] = inv
					if objets[inv]['use']:
						usable_inv.append("(x"+str(rick['inventaire'][inv])+")"+str(objets[inv]['description']))
				table_inv.append(consommable_inv)
				table_inv.append(usable_inv)
				table_=AsciiTable(table_inv)
				print table_.table
						#RINV[objets[inv]['description']] = inv

				print "Voulez vous utiliser quelque chose de votre inventaire?"
				qinv_id,qinv_name = self.action(["Oui","Non"])
				if qinv_id == 0:
					print "Quel objet?"
					inv_id,inv_name = self.action(RINV.keys())
					rick['inventaire'][RINV[inv_name]]-=1
					if rick['inventaire'][RINV[inv_name]]<=0:
						del rick['inventaire'][RINV[inv_name]]
					self.effect_object(RINV[inv_name])
			else:
				print "Votre inventaire est vide!"


		if chx_id == 2:
			if chx_name == "Discuter...":
				print "Discuter avec qui ?:"
				spk_id,spk_name = self.get_local_social()
				print "Vous parlez maintenant à:",spk_name
				RESADV = self.get_speak(spk_id+1,spk_name)
				if RESADV == -2:
					if rick['in'] == "XZ-321":
						if self.position == 'dans un coin':
							GROUP.append(spk_id+1)
							#print ">>>>>>>>>GROUP: ",GROUP
							self.next_adv()
							rick['pisto-portail'] = 1
							DIMBLOCKED.remove("D-634")
							lieux[rick['in']]['dans un coin']['pnj'].remove(spk_id+1)


				if RESADV == -1:
					if rick['in'] == "XZ-321":
						if self.position == 'bar':
							print "Nouvelle position débloquée! :",BLOCKED[rick['in']]
							del BLOCKED[rick['in']]
				if RESADV == 0:
					print bcolors.OKBLUE+str("VOUS VENEZ DE COMMETTRE UN MEURTRE!")+bcolors.ENDC
				if RESADV == 1:
					print  bcolors.FAIL+str("VOUS VENEZ DE BLESSER QUELQU'UN!")+bcolors.ENDC
				if RESADV == None:
					print "VOUS AVEZ BIEN PARLER."
				if RESADV == "depanne80":
					if rick['schmeckles']>150:
						print "Vous dite que finalement vous n'en avez pas besoin."
					else:
						rick['schmeckles'] += 80
				if RESADV == "biereall1":
					if "XY-422" in DIMBLOCKED:
						if (1 in rick['inventaire'].keys()) or (2 in rick['inventaire'].keys()):
							if (1 in rick['inventaire'].keys()):
								self.sub_to_inv(1)
							else:
								self.sub_to_inv(2)
							print "VOUS DONNER UNE BIERE."
							print "VOUS DEBLOQUER UNE DIMENSSION: XY-422"
							DIMBLOCKED.remove("XY-422")
						else:
							print "VOUS N'AVEZ PAS DE BIERE DANS VOTRE INVENTAIRE!"
				if RESADV == "adventure1":
					if "F-352" in DIMBLOCKED:
						if "7" in GROUP:
							print "VOUS DEBLOQUEZ UNE DIMENSSION: ", "F-352"
							DIMBLOCKED.remove("F-352")
						else:
							print "Vous n'etes pas avec morty! il vous faut un morty!"



				if type(RESADV) == str:
					exec("self."+RESADV+"()")

			else:
				chx_id = 3

		if chx_name == "Pisto-portail...":
			print "Voici les endrois possibles:"
			mve_id,mve_name = self.get_dim_move()
			print "Vous êtes maintenant à:",bcolors.BOLD+str(mve_name)+bcolors.ENDC
			rick['in'] = mve_name
			self.position = None
			if mve_name == "D-634": 
				print "Vous êtes maintenant chez vous!\nVous pouvez fabriquer ici tout un tas de truc!\nAllez dans d'autres dimensions pour collecter des ressources!"
			else:
				print "Ce monde est a explorer, mais il est possible qu'il n'y a plus rien a faire..."

	def uplowPV(self):
		#simple non ?
		self.effect_object(1)

	def uplowEXP(self):
		REXP = 5
		self.rick_expup(REXP)

	def uphighPV(self):
		#si vous comprenez pas je peut rien faire pour vous ...
		self.effect_object(3)

	def death(self):
		print bcolors.WARNING+"VOUS ETES MORT !\nFIN DU JEU..."+bcolors.ENDC
		zzz=raw_input("")
		sys.exit(0)

	def effect_object(self,id_):
		print "VOUS UTILISEZ: ",objets[id_]['description']
		if id_ == 1:
			RPV = 10
			cur_pv,max_pv = rick['PV']['current'],rick['PV']['max']
			max_restore = max_pv-cur_pv
			if max_restore>RPV:
				restore_pv = RPV
			else:
				restore_pv = max_restore
			rick['PV']['current'] += restore_pv
			print "VOUS AVEZ RESTAURER ",bcolors.OKGREEN+str(restore_pv)+bcolors.ENDC, "PV !"
		if id_ == 2:
			RPV = 15
			cur_pv,max_pv = rick['PV']['current'],rick['PV']['max']
			max_restore = max_pv-cur_pv
			if max_restore>RPV:
				restore_pv = RPV
			else:
				restore_pv = max_restore
			rick['PV']['current'] += restore_pv
			print "VOUS AVEZ RESTAURER ",bcolors.OKGREEN+str(restore_pv)+bcolors.ENDC, "PV !"
		if id_ == 3:
			RPV = 35
			cur_pv,max_pv = rick['PV']['current'],rick['PV']['max']
			max_restore = max_pv-cur_pv
			if max_restore>RPV:
				restore_pv = RPV
			else:
				restore_pv = max_restore
			rick['PV']['current'] += restore_pv
			print "VOUS AVEZ RESTAURER ",bcolors.OKGREEN+str(restore_pv)+bcolors.ENDC, "PV !"
		if id_ == 4:
			REXP = 10
			self.rick_expup(REXP)

		if id_ == 17:
			#EFFETS DE LA XINOCAINE
			SANG_TIMERS.append(Timer(randint(60*3,60*5),self.stop_effects,args=[id_]))
			SANG_TIMERS[-1].start()
			SANG.append(id_)
			print bcolors.OKGREEN+"VOUS ETES SUPER FORT PENDANT QUELQUES HEURES..."+bcolors.ENDC
			rick['atk'] += 35 
			rick['def'] += 25

		if id_ == 18:
			#EFFETS DE XEERDER
			SANG.append(id_)
			print bcolors.OKGREEN+"VOUS GAGNER EN ESPERANCE DE VIE..."+bcolors.ENDC
			rick['PV']['max'] += 5

		if id_ == 19:
			#Effets du Ganax
			rick['PV']['max'] -=2
			rick['PV']['current'] = rick['PV']['max']
			rick['def'] = abs(rick['def'] - 1)

	def stop_effects(self,id_):
		SANG.remove(id_)
		if id_ == 17:
			rick['atk'] -= 35 
			rick['def'] -= 25

	def rick_expup(self,REXP):
		rick['EXP'],REXP = (rick['EXP']+(REXP)/rick['level']) % 100, (rick['EXP']+(REXP)/rick['level'])
		print "VOUS AVEZ ETES A ",str(REXP)+"%", "d'EXP !"
		if rick['EXP'] == 0:
			print bcolors.HEADER+str("VOUS AVEZ GAGNER UN NIVEAU D'EXP!")+bcolors.ENDC
			self.rick_levelup()

	def rick_levelup(self):
		random.seed(datetime.now())
		rick['level'] += 1
		ATK,DEF = random.randint(1,6),random.randint(1,4)
		print "VOUS GAGNEZ +"+bcolors.HEADER+str(ATK)+bcolors.ENDC+" ATK et "+bcolors.HEADER+str(DEF)+bcolors.ENDC+" DEF !"
		rick['atk']+=ATK
		rick['def']+=DEF

	def gain_exp(self,damage):
		return math.pow(math.log(damage*3+1),2)

	def get_speak(self,id_,name_):	
		print bcolors.BOLD+"VOUS COMMENCEZ UNE CONVERSATION AVEC:"+bcolors.ENDC,name_
		while True:
			cur = dialogues[id_]['current']
			max_=max(dialogues[id_]['dials'].keys())
			if cur in dialogues[id_]['dials'].keys():
				msg = dialogues[id_]['dials'][cur]['text']
				reps = dialogues[id_]['dials'][cur]['reponses']
				dialogues[id_]['dials'][cur]['reponses']["Rien a foutre, je me barre."] = "quit"
				rndreps =  list(reps.keys())

				#rndreps = random.shuffle(rkeys)
				print '[',name_,'] Dit:',msg
				rep_id,rep_name = self.action_conv(rndreps,rnd=1)
				print "Vous lui dites:",rep_name
				if dialogues[id_]['dials'][cur]['reponses'][rep_name] == "quit":
					print bcolors.BOLD+"Vous quittez la conversation..."+bcolors.ENDC
					return None


				if dialogues[id_]['dials'][cur]['reponses'][rep_name] == "good":
					if cur+1 != 0:
						if cur+1 in dialogues[id_]['dials'].keys():
							dialogues[id_]['current']+=1
						else:
							if -1 in dialogues[id_]['dials'].keys():
								dialogues[id_]['current'] = -1
								print bcolors.BOLD+"Vous quittez la conversation.\nMission accomplie!"+bcolors.ENDC
								return personnages[id_]['quete']

							if dialogues[id_]['current'] == max_:
								print bcolors.BOLD+"Vous quittez la conversation!"+bcolors.ENDC
								return personnages[id_]['quete']

				if dialogues[id_]['dials'][cur]['reponses'][rep_name] == "bad":
					print '[',name_,'] Réponds:',dialogues[id_]['dials'][cur]['bad']

				if dialogues[id_]['dials'][cur]['reponses'][rep_name] == "quest":
					#ici le code de la quête
					for inv in personnages[id_]['inventaire'].keys():
						if inv in rick['inventaire'].keys():
							rick['inventaire'][inv] += personnages[id_]['inventaire'][inv]
						else:
							rick['inventaire'][inv] = personnages[id_]['inventaire'][inv]
						print "Vous recevez: +",bcolors.OKGREEN+str(personnages[id_]['inventaire'][inv])+bcolors.ENDC,bcolors.OKGREEN+objets[inv]['description']+bcolors.ENDC
					print '[',name_,'] Réponds: Au revoir...'
					lieux[rick['in']][self.position]['pnj'].remove(id_)
					del dialogues[id_]
					return -3
					
				if dialogues[id_]['dials'][cur]['reponses'][rep_name] == "kill":
					return self.combat_loop(id_)

	def combat_loop(self,id_):
		INITPVIA = personnages[id_]['PV']

		BOT = IA(personnages[id_],id_)
		while True:
			if rick['PV']['current'] < 0:
				break
			print "--MODE COMBAT--"

			if rick['PV']['current']<rick['PV']['max']/2.0:
				print "VOUS AVEZ",bcolors.WARNING+str(rick['PV']['current'])+bcolors.ENDC,"/",rick['PV']['max'],"PV"#'PV': {'current':19,'max':25}
			else:
				print "VOUS AVEZ",rick['PV']['current'],"/",rick['PV']['max'],"PV"#'PV': {'current':19,'max':25}
		
			print "VOUS ETES NIVEAU:",bcolors.HEADER+str(rick['level'])+bcolors.ENDC

			idact,action = self.action(["attaquer","fuir","objet"])

			persondie,turndie = BOT.simulation_very_basic()
			if persondie == "me":
				print personnages[id_]['nom'], ":Pense qu'il va mourrir."
			if persondie == "rick":
				print personnages[id_]['nom'], ":Pense qu'il va vous éclater la gueule."

			choix_ia = BOT.battle_act(persondie,turndie)
			if choix_ia == 1:
				print "VOUS ETES ATTAQUEZ PAR",bcolors.BOLD+personnages[id_]['nom']+bcolors.ENDC
				rick['PV']['current']-=self.damage(personnages[id_],rick)
			

			#ici le code d'échec %
			#affecter rick et le pnj
			if action == "attaquer":
				print "VOUS ATTAQUEZ ",bcolors.BOLD+personnages[id_]['nom']+bcolors.ENDC
				dmgs = self.damage(rick,personnages[id_])
				personnages[id_]['PV']-=dmgs

				if personnages[id_]['PV']<=0:
					#si ça reussit
					lieux[rick['in']][self.position]['pnj'].remove(id_)
					del dialogues[id_]
					#gain exp
					self.rick_expup(self.gain_exp(dmgs))
					#ici le code de drop des gains
					rick['schmeckles'] += personnages[id_]['schmeckles']
					del personnages[id_]
					return 0

			
			if action == "fuir":	
				chance = random.randint(0,10)
				if chance:
					personnages[id_]['PV'] = INITPVIA
					return 1
				print "VOUS N'AVEZ PAS REUSSIT A FUIR"
				TOURPASS = True

			if action == "objet":
				pass # ici le choix d'un objet (tout les 4 tours)


from copy import deepcopy
class IA(object):
	class rick:
		level = 1
		EXP = 0
		PV = 20
		schmeckles = 0
		inventaire = {}
		ATK = 5
		DEF = 5
		arme = 0
		armure = 0
		equipement = []
		max_atk = 5

	def __init__(self,personnage_data,id_):
		personnage_data = deepcopy(personnage_data)
		self.id=id_
		self.PV,self.schmeckles,self.race = personnage_data['PV'],personnage_data['schmeckles'],personnage_data['race']
		self.ATK = personnage_data['atk']
		self.DEF = personnage_data['def']
		self.inventaire = personnage_data['inventaire']
		self.maxPV = self.PV

		self.armes = []
		self.max_atk = self.ATK
		for i in self.inventaire.keys():
			if objets[i]['weapon']:
				if objets[i]['atk']>self.max_atk:
					self.max_atk = objets[i]['atk']
				self.armes.append(i)
		
		self.rick.level = deepcopy(rick['level'])
		self.rick.EXP = deepcopy(rick['EXP'])
		self.rick.PV = deepcopy(rick['PV'])
		self.rick.schmeckles = deepcopy(rick['schmeckles'])
		self.rick.inventaire = deepcopy(rick['inventaire'])
		self.rick.ATK = deepcopy(rick['atk'])
		self.rick.DEF = deepcopy(rick['def'])
		self.rick.arme = deepcopy(rick['arme'])
		self.rick.armure = deepcopy(rick['armure'])
		self.rick.equipement = deepcopy(rick['equipement'])
		if self.rick.arme != 0:
			if self.rick.ATK>objets[self.rick.arme]['atk']: self.rick.max_atk = self.rick.ATK
			if self.rick.ATK<objets[self.rick.arme]['atk']: self.rick.max_atk = objets[self.rick.arme]['atk']
		else:
			self.rick.max_atk = self.rick.ATK

	def is_good_for_me(self):
		return self.PV/float(self.maxPV) > 0.7

	def is_bad_for_me(self):
		return self.PV/float(self.maxPV) < 0.35

	def damage(self,a_atk,t_def):
		#ATK.a / 2 - DEF.t / 4 = degat infligé.
		#ATK.a : Attaque de l'attaquant
		#DEF.t : Defense de la cible
		damage = a_atk/2.0 - t_def/4.0
		#print bcolors.WARNING+str(damage)+bcolors.ENDC+" DEGATS POUR "+bcolors.BOLD+t['nom']+bcolors.ENDC
		return damage


	def effect_object(self,id_):
		print "IL UTILISE: ",objets[id_]['description']
		if id_ == 1:
			RPV = 10
			cur_pv,max_pv = self.PV, self.maxPV
			max_restore = max_pv-cur_pv
			if max_restore>RPV:
				restore_pv = RPV
			else:
				restore_pv = max_restore
			self.PV += restore_pv
			print "IL A RESTAURER ",bcolors.OKGREEN+str(restore_pv)+bcolors.ENDC, "PV !"
		if id_ == 2:
			RPV = 15
			cur_pv,max_pv = self.PV, self.maxPV
			max_restore = max_pv-cur_pv
			if max_restore>RPV:
				restore_pv = RPV
			else:
				restore_pv = max_restore
			self.PV += restore_pv
			print "IL A RESTAURER ",bcolors.OKGREEN+str(restore_pv)+bcolors.ENDC, "PV !"
		if id_ == 3:
			RPV = 35
			cur_pv,max_pv = self.PV, self.maxPV
			max_restore = max_pv-cur_pv
			if max_restore>RPV:
				restore_pv = RPV
			else:
				restore_pv = max_restore
			self.PV += restore_pv
			print "IL A RESTAURER ",bcolors.OKGREEN+str(restore_pv)+bcolors.ENDC, "PV !"

		self.inventaire[id_] -= 1
		if self.inventaire[id_] < 1:
			del self.inventaire[id_]


	def battle_act(self,persondie,turndie):#ici le code qui execute l'algorithme de l'IA
		if persondie == "me":
			if self.PV/float(self.maxPV) < 0.4:
				if 1 in self.inventaire.keys():
					self.effect_object(1)
					return 0
				if 2 in self.inventaire.keys():
					self.effect_object(2)
					return 0
				if 3 in self.inventaire.keys():
					self.effect_object(3)
					return 0
				return 1
			else:
				return 1	
		if persondie == "rick":
			return 1


	def simulation_very_basic(self):
		simulation = deepcopy(self)
		max_atk_rick,max_atk_me = simulation.rick.max_atk,simulation.max_atk
		#print "SIMULATION:"
		turn = {'me':[],'rick':[]}
		while ((simulation.PV >= 1) and (simulation.rick.PV['current'] >= 1)):
			action = {}
			action['act'] = "atk"
			action['atk'] = max_atk_rick
			action['def'] = simulation.rick.DEF
			turn['rick'].append(action)
			dmg = self.damage(action['atk'],simulation.DEF)
			simulation.PV -= dmg
			#print "RICK - TOUR #",len(turn['rick'])
			#print "Rick me fait mal: -"+str(dmg)
			#print "PV: ",simulation.PV

			print "\n"

			if simulation.PV >= 1:
				action = {}
				action['act'] = "atk"
				action['atk'] = max_atk_me
				action['def'] = simulation.DEF
				turn['me'].append(action)
				dmg = self.damage(action['atk'],simulation.rick.DEF)
				simulation.rick.PV['current'] -= dmg
				#print "IA - TOUR #",len(turn['me'])
				#print "Je lui fait mal: -"+str(dmg)
				#print "PV: ",simulation.rick.PV

		if int(simulation.PV) <= 0:
			#print "JE SUIS MORT EN: ",len(turn['me']),"TOURS"
			return "me",len(turn['me'])

		if int(simulation.rick.PV['current']) <= 0:
			#print "IL EST MORT EN: ",len(turn['rick']),"TOURS"
			return "rick",len(turn['rick'])


"""
sys.stdout.write("\x1b[8;80;130t")
from time import sleep
logo = read_file("gameressource/asciilogo.$")
print logo
sleep(10)
"""
Clean()

Jeu = Game()
Jeu.start()





